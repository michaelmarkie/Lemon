
## Lemon:
# Minimal Code Javascript Framework - No BS
# License: DBAD http://www.dbad-license.org
## By:
# Michael Markie - michaelmarkie.co.uk

# Usage:
# var element = new lemon('#element')
#   element.css('background','red')
# (new lemon('#element')).css('background','green')
# l('#element').css('background','blue')



Array::remove = (from, to) -> #http://stackoverflow.com/a/18539905 - temporary
    @splice(from, if (to=[0,from||1,++to-from][arguments.length])<0 then @length+to else to)
    @length

lemon = (el, context = document ) ->

    @selector = el;

    if context instanceof lemon
        context = context[0][0]

    else if typeof context is 'string'
        context = document.querySelector context


    switch typeof el
        when 'string'
            @[0] = context.querySelectorAll el

        when 'element', 'object'
            @[0] = []
            @[0][0] = el

        when 'HTMLCollection', 'NodeList'
            @[0] = el

        when 'function' 
            window.lemon_onload = window.onload
            window.onload = ->
                if window.lemon_onload?
                    window.lemon_onload()
                el()
                return

    @

lemon:: =
    events: []
    # I found it important to include as I may want to chain a bunch of call and include a element switch on the change.
    # I'm torn with recalling a new session.
    find: (el) ->
        @[0] = @[0][0].querySelectorAll el
        
        @
    # Grab the element[s] that we're storing.
    get: (first = true)->
        if first then @[0][0] else @[0]
            
    # Very important function which is used throughout the script on most calls.
    each: (callback) ->
        for element in @[0]
            if !callback.apply element
                @
            
        @
    css: (key, value) ->
        # I am a fan of objects when changing alot of css elements
        if typeof key is 'object'
            for k,v of key
                @sing 'style', k, v
            @
        else
            @sing 'style', key, value
            
    width: ->
        @get().offsetWidth
            
    attr: (key, value) -> 
        if value?
            @sing 'setAttribute', [key, value]
        else
            @sing 'getAttribute', [key]
    data: (key, value) ->
        @attr 'data-' + key, value
    remove_attr: (key) ->
        @sing 'removeAttribute', [key]
        @
    position: ->
        @[0][0].getBoundingClientRect()
        
    parent: (closest) ->
        if closest?
            el = @[0][0]
            match = true
            while match
                el = el.parentNode
                if !el
                    @[0][0] = document.createElement 'div'
                    match = false
                if el.matches closest
                    @[0][0] = el
                    match = false
        else
            @[0][0] = @[0][0].parentNode
        @
    show: -> @sing 'style', 'display', ''
    hide: -> @sing 'style', 'display', 'none'
    append_child: (v) ->
        @sing 'appendChild', v
        @
        
    prepend: (html) -> @insert 'afterbegin', html
    before: (html) -> @insert 'beforebegin', html
    append: (html) -> @insert 'beforeend', html
    after: (html) -> @insert 'afterend', html
    insert: (where, html) -> @sing 'insertAdjacentHTML', [where, html]
    text: (str) ->
        if str?
            @sing 'innerText', str
        else
            @[0][0].innerText
    html: (str) ->
        if str?
            @sing 'innerHTML', str
        else
            @[0][0].innerHTML
    val: (str) -> 
        if str?
            @sing 'value', str
        else
            @[0][0].value
    remove: (replace = '') -> @sing 'outerHTML', replace
    empty: -> @sing 'innerHTML', ''
    add_class: (cls) -> 
        @sing 'classList', 'add', [cls]
        @
    remove_class: (cls) ->
        @sing 'classList', 'remove', [cls]
        @
    classes: ->
        Array.from(@get().classList)
        
    #Do it all function, this processes most of the internal call's
    sing: (args...) ->
        # Pop the last element as that'll be our value
        value = args.pop()
        # We need to retain the element itself not collect the data thats in the object 
        retain = args.pop()

        for element in @[0]
            
            for key in args
                element = element[key]
            
            if typeof element[retain] is 'function'
                # Don't want to return anything if our arguments consist of more than 1 item as to assume that would be used to set something and have no use to us
                if value.length > 1 || @[0].length > 1
                
                    element[retain].apply element, value
                    
                else if typeof value
                
                    return element[retain].apply element, value
                    
            else
                element[retain] = value
                
        return @
        
    trigger: (event) ->
        @each ->
            @dispatchEvent event
            
        @
        
    # Our simple function for watching events
    on: (event, args...) ->

        for arg in args
            if typeof arg is 'function'
                callback = arg
            else if typeof arg is 'string'
                watch = arg
        
        _this = @
        
        @each ->
        
            cb = (e) ->
                if watch?
                    if e.target and e.target.matches watch
                        callback.apply e.target, [e]
                else
                    callback.apply e.target, [e]
                return
        
            _this.events.push 
                type: event
                callback: cb
                selector: _this.selector+''
        
            @addEventListener event, cb
            return

        @
    
    remove_event: (event) ->
            
        for obj,i in @events
            if obj.selector == @selector+'' && obj.type == event
                @each ->
                    @removeEventListener obj.type, obj.callback
                @events.remove i
                    
        @
                

window.l = l = ->
    args = [].slice.call(arguments)
    args.unshift(null)
    new (Function.bind.apply( lemon, args ))()