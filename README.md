# Lemon Library
Javascript compact library, quick and easy to use with minimal bloat.

### Usage:

```
var element = new lemon('#element')
    element.css('background','red')
```

`(new lemon('#element')).css('background','green')`

`l('#element').css('background','blue')`

### Functions:
#### Style:
* `css(key | object, [value])`
* `show()`
* `hide()`
* `add_class()`
* `remove_class()`
* `classes()` - Return an array of classes

#### Attributes:
* `attr(key, [value])`
* `data(key, [value])`
* `remove_attr(key)`

#### Data:
* `text([text])` - Set or get inner Text
* `html([html])` - Set or get inner HTML
* `val([key])` - Change value attribute
* `prepend(html)` - Prepend inside element
* `append(html)` - Append inside element
* `before(html)` - Put before element open
* `after(html)` - Put after element closure
* `remove([replace])` - Remove element and contents [replace with html]
* `empty()` - Remove contents in element

#### Events:
* `on(event, [element, callback])`
* `trigger(event)`
* `remove_event(event)`
    
### FAQ's

**Will animations be added?** No they can be done more efficiently in CSS

**Why Lemon?** I like lemons.
